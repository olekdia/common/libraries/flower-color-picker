package com.olekdia.flowercolorpicker

interface OnColorChangedListener {
    fun onColorChanged(newColor: Int)
}