package com.olekdia.flowercolorpicker.slider

interface OnValueChangedListener {
    fun onValueChanged(value: Float)
}