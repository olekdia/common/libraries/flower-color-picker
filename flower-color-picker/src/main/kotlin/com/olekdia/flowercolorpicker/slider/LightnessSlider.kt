package com.olekdia.flowercolorpicker.slider

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.common.extensions.lightness
import com.olekdia.common.extensions.withLightness
import kotlin.math.max

class LightnessSlider @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : AbsCustomSlider(context, attrs, defStyleAttr) {

    private var color: Int = Color.TRANSPARENT
    private val barPaint: Paint = Paint()
    private val solidPaint: Paint = Paint()
    private val clearingStrokePaint: Paint = Paint().apply {
        this.color = Color.WHITE
        this.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }

    override fun Canvas.drawBar() {
        val width: Int = this.width
        val height: Int = this.height
        val hsv = FloatArray(3)
        Color.colorToHSV(color, hsv)
        val l: Float = max(2, width / 256).toFloat()
        var x: Float = 0f
        while (x <= width) {
            hsv[2] = x / (width - 1)
            barPaint.color = Color.HSVToColor(hsv)
            this.drawRect(x, 0f, x + l, height.toFloat(), barPaint)
            x += l
        }
    }

    override fun onValueChanged(value: Float) {
        colorPicker?.setLightness(value)
    }

    override fun drawHandle(
        canvas: Canvas,
        x: Float,
        y: Float
    ) {
        solidPaint.color = color.withLightness(value)
        canvas.drawCircle(x, y, handleRadius.toFloat(), clearingStrokePaint)
        canvas.drawCircle(x, y, handleRadius * 0.75f, solidPaint)
    }

    fun setColor(color: Int) {
        this.color = color
        value = color.lightness
        if (bar != null) {
            updateBar()
            invalidate()
        }
    }
}