package com.olekdia.flowercolorpicker.slider

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.os.Build
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.PointerIcon
import android.view.View
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.flowercolorpicker.ColorPickerView
import com.olekdia.flowercolorpicker.R
import kotlin.math.max
import kotlin.math.min

abstract class AbsCustomSlider @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : View(context, attrs, defStyleAttr) {

    var colorPicker: ColorPickerView? = null

    private var bitmap: Bitmap? = null
    private var bitmapCanvas: Canvas? = null
    protected var bar: Bitmap? = null
    private var barCanvas: Canvas? = null
    var onValueChangedListener: OnValueChangedListener? = null
    private var barOffsetX = 0
    protected var handleRadius = 20
    protected var barHeight = 5
    protected var value = 1f
        set(newValue) {
            field = max(0f, min(newValue, 1f))
        }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pointerIcon = PointerIcon.getSystemIcon(context, PointerIcon.TYPE_HAND)
        }
    }

    protected fun updateBar() {
        handleRadius = resources.getDimensionPixelSize(R.dimen.fcp_color_slider_handler_radius)
        barHeight = resources.getDimensionPixelSize(R.dimen.fcp_color_slider_bar_height)
        barOffsetX = handleRadius
        if (bar == null) createBitmaps()
        barCanvas?.drawBar()
        invalidate()
    }

    protected open fun createBitmaps() {
        val w: Int = width
        val h: Int = height
        bar = Bitmap.createBitmap(max(w - barOffsetX * 2, 1), barHeight, Bitmap.Config.ARGB_8888)
            .also {
                barCanvas = Canvas(it)
            }

        bitmap.let { bmp: Bitmap? ->
            if (bmp == null
                || bmp.width != w
                || bmp.height != h
            ) {
                bitmap?.recycle()
                bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
                    .also {
                        bitmapCanvas = Canvas(it)
                    }
            }
        }
    }

    protected abstract fun Canvas.drawBar()
    protected abstract fun drawHandle(
        canvas: Canvas,
        x: Float,
        y: Float
    )

    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        super.onWindowFocusChanged(hasWindowFocus)
        if (width > 0 && height > 0) updateBar()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width: Int = when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.UNSPECIFIED -> widthMeasureSpec
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(widthMeasureSpec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(widthMeasureSpec)
            else -> heightMeasureSpec
        }
        val height: Int = when (MeasureSpec.getMode(heightMeasureSpec)) {
            MeasureSpec.UNSPECIFIED -> heightMeasureSpec
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(heightMeasureSpec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(heightMeasureSpec)
            else -> heightMeasureSpec
        }
        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        ifNotNull(
            bar, bitmap, bitmapCanvas
        ) { bar, bmp, bmpCanvas ->
            bmpCanvas.drawColor(0, PorterDuff.Mode.CLEAR)
            bmpCanvas.drawBitmap(
                bar,
                barOffsetX.toFloat(),
                (height - bar.height) * .5f,
                null
            )
            val x: Float = handleRadius + value * (width - handleRadius * 2)
            val y: Float = height * .5f
            drawHandle(bmpCanvas, x, y)
            canvas.drawBitmap(bmp, 0f, 0f, null)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                parent?.requestDisallowInterceptTouchEvent(true)
                onUpdateValueAfterEvent(event.x)
            }
            MotionEvent.ACTION_MOVE -> {
                onUpdateValueAfterEvent(event.x)
            }
            MotionEvent.ACTION_UP -> {
                onValueChanged()
            }
        }
        return true
    }

    private fun onValueChanged() {
        onValueChanged(value)
        onValueChangedListener?.onValueChanged(value)
        colorPicker?.run {
            onColorSelected()
            hideInputKeyboard()
        }
        this.requestFocus()
        invalidate()
    }

    protected abstract fun onValueChanged(value: Float)

    private fun onUpdateValueAfterEvent(eventX: Float) {
        bar?.let {
            value = (eventX - barOffsetX) / it.width
        }
        onValueChanged(value)
        invalidate()
    }

    /**
     * Important to intercept it here to avoid focus change
     */
    override fun dispatchKeyEvent(event: KeyEvent?): Boolean =
        when (event?.keyCode){
            KeyEvent.KEYCODE_DPAD_LEFT -> {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    value -= 0.01f
                    onValueChanged()
                }
                true
            }
            KeyEvent.KEYCODE_DPAD_RIGHT -> {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    value += 0.01f
                    onValueChanged()
                }
                true
            }
            else -> super.dispatchKeyEvent(event)
        }
}