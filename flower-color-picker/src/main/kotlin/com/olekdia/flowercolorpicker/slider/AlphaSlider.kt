package com.olekdia.flowercolorpicker.slider

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.common.extensions.alpha
import com.olekdia.common.extensions.toFloatColorComponent
import com.olekdia.flowercolorpicker.ColorPickerView
import kotlin.math.max
import kotlin.math.roundToInt

class AlphaSlider @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : AbsCustomSlider(context, attrs, defStyleAttr) {

    private var color: Int = Color.TRANSPARENT
    private val alphaPatternPaint: Paint = Paint()
    private val barPaint: Paint = Paint()
    private val solidPaint: Paint = Paint()
    private val clearingStrokePaint: Paint = Paint().apply {
        this.color = Color.WHITE
        this.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }

    override fun createBitmaps() {
        super.createBitmaps()
        alphaPatternPaint.shader = ColorPickerView.createAlphaPatternShader(
            barHeight / 2
        )
    }

    override fun Canvas.drawBar() {
        val width: Int = this.width
        val height: Int = this.height
        this.drawRect(0f, 0f, width.toFloat(), height.toFloat(), alphaPatternPaint)
        val l: Float = max(2, width / 256).toFloat()
        var alpha: Float
        var x: Float = 0f
        while (x <= width) {
            alpha = x / (width - 1)
            barPaint.color = color
            barPaint.alpha = (alpha * 255).roundToInt()
            this.drawRect(x, 0f, x + l, height.toFloat(), barPaint)
            x += l
        }
    }

    override fun onValueChanged(value: Float) {
        colorPicker?.setAlphaValue(value)
    }

    override fun drawHandle(
        canvas: Canvas,
        x: Float,
        y: Float
    ) {
        solidPaint.color = color
        solidPaint.alpha = Math.round(value * 255)
        canvas.drawCircle(x, y, handleRadius.toFloat(), clearingStrokePaint)
        if (value < 1) {
            canvas.drawCircle(x, y, handleRadius * 0.75f, alphaPatternPaint)
        }
        canvas.drawCircle(x, y, handleRadius * 0.75f, solidPaint)
    }

    fun setColor(color: Int) {
        this.color = color
        value = color.alpha.toFloatColorComponent()
        if (bar != null) {
            updateBar()
            invalidate()
        }
    }
}