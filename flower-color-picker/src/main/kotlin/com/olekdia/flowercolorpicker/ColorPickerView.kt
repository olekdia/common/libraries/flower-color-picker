package com.olekdia.flowercolorpicker

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.os.Parcelable
import android.text.InputFilter.AllCaps
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.IntDef
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.*
import com.olekdia.androidcommon.interfaces.OnEditorEnterListener
import com.olekdia.common.extensions.*
import com.olekdia.flowercolorpicker.renderer.ColorWheelRenderOption
import com.olekdia.flowercolorpicker.renderer.ColorWheelRenderer
import com.olekdia.flowercolorpicker.renderer.FlowerColorWheelRenderer
import com.olekdia.flowercolorpicker.renderer.SimpleColorWheelRenderer
import com.olekdia.flowercolorpicker.slider.AlphaSlider
import com.olekdia.flowercolorpicker.slider.LightnessSlider
import kotlin.math.cos
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.math.sin

@Suppress("unused", "WeakerAccess")
class ColorPickerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : View(context, attrs, defStyleAttr),
    OnEditorEnterListener {

    private var colorWheel: Bitmap? = null
    private var colorWheelCanvas: Canvas? = null
    private var density = 10
    private var lightness = 1f
    private var _alpha = 1f
    private val _backgroundColor = Color.TRANSPARENT
    var initialColor: Int = Color.WHITE
        set(color) {
            _alpha = color.alpha.toFloatColorComponent()
            lightness = FloatArray(3)
                .also { Color.colorToHSV(color, it) }
                .get(2)
            currentColorCircle = findNearestByColor(color)
            currentColor = color
            field = color
            setColorToSliders(color)
            setColorToEditText(color)
            updateColorWheel()
        }
    private val colorWheelFill: Paint = Paint()
    private val selectorStroke1: Paint = Paint()
    private val selectorStroke2: Paint = Paint()
    private val alphaPatternPaint: Paint = Paint()
    private var currentColorCircle: ColorCircle? = null
    var onColorChangedListener: OnColorChangedListener? = null
    private var lightnessSlider: LightnessSlider? = null
    private var alphaSlider: AlphaSlider? = null
    private var colorEdit: EditText? = null
    private var renderer: ColorWheelRenderer? = null
    private var alphaSliderViewId = NO_RESOURCE
    private var lightnessSliderViewId = NO_RESOURCE
    private var colorEditViewId = NO_RESOURCE
    // Current color that selected in picker
    var currentColor: Int = Color.WHITE
        private set
    // Current color that selected  in picker, according to the picker itself. May lose accuracy
    val selectedColorInPicker: Int
        get() = (currentColorCircle?.let {
            Color.HSVToColor(it.getHsvWithLightness(lightness))
        } ?: run {
            Color.WHITE
        }).withAlpha(_alpha)


    val isAlphaEnabled: Boolean get() = alphaSlider.isNotNullAndVisible()

    val isLightnessEnabled: Boolean get() = lightnessSlider.isNotNullAndVisible()

    init {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ColorPickerView)
        colorWheelFill.color = Color.TRANSPARENT
        selectorStroke1.color = Color.WHITE
        selectorStroke2.color = Color.BLACK

        density = a.getInt(R.styleable.ColorPickerView_density, 10)
        val initColor: Int = a.getInt(R.styleable.ColorPickerView_initialColor, Color.WHITE)
        @WheelType val wheelType: Int = a.getInt(
            R.styleable.ColorPickerView_wheelType,
            WheelType.FLOWER
        )
        val renderer: ColorWheelRenderer = getRenderer(wheelType)
        alphaSliderViewId = a.getResourceId(R.styleable.ColorPickerView_alphaSliderView, NO_RESOURCE)
        lightnessSliderViewId = a.getResourceId(R.styleable.ColorPickerView_lightnessSliderView, NO_RESOURCE)
        colorEditViewId = a.getResourceId(R.styleable.ColorPickerView_colorEditView, NO_RESOURCE)
        setRenderer(renderer)
        setDensity(density)
        initialColor = initColor
        a.recycle()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (alphaSliderViewId != NO_RESOURCE) {
            setAlphaSlider(
                rootView.findViewById<View>(alphaSliderViewId) as? AlphaSlider?
            )
        }
        if (lightnessSliderViewId != NO_RESOURCE) {
            setLightnessSlider(
                rootView.findViewById<View>(lightnessSliderViewId) as? LightnessSlider?
            )
        }
        if (colorEditViewId != NO_RESOURCE) {
            setColorEdit(
                rootView.findViewById<View>(colorEditViewId) as? EditText?
            )
        }
    }

    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        super.onWindowFocusChanged(hasWindowFocus)
        updateColorWheel()
        currentColorCircle = findNearestByColor(currentColor)
    }

    override fun onLayout(
        changed: Boolean,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int
    ) {
        super.onLayout(changed, left, top, right, bottom)
        updateColorWheel()
        currentColorCircle = findNearestByColor(currentColor)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateColorWheel()
    }

    private fun updateColorWheel() {
        var w: Int = measuredWidth
        val h: Int = measuredHeight
        if (h < w) w = h
        if (w <= 0) return

        colorWheel.let { wheel ->
            if (wheel == null || wheel.width != w) {
                colorWheel = Bitmap.createBitmap(w, w, Bitmap.Config.ARGB_8888)
                    .also {
                        colorWheelCanvas = Canvas(it)
                    }
                alphaPatternPaint.shader = createAlphaPatternShader(8)
            }
        }

        drawColorWheel()
        invalidate()
    }

    private fun drawColorWheel() {
        ifNotNull(
            colorWheelCanvas, renderer
        ) { wheelCanvas, renderer ->
            wheelCanvas.drawColor(0, PorterDuff.Mode.CLEAR)
            val half: Float = wheelCanvas.width * .5f
            val strokeWidth: Float = STROKE_RATIO * (1f + ColorWheelRenderer.GAP_PERCENTAGE)
            val maxRadius: Float = half - strokeWidth - half / density
            val cSize: Float = maxRadius / (density - 1) / 2
            val colorWheelRenderOption: ColorWheelRenderOption = renderer.renderOption
            colorWheelRenderOption.density = density
            colorWheelRenderOption.maxRadius = maxRadius
            colorWheelRenderOption.cSize = cSize
            colorWheelRenderOption.strokeWidth = strokeWidth
            colorWheelRenderOption.alpha = _alpha
            colorWheelRenderOption.lightness = lightness
            colorWheelRenderOption.targetCanvas = colorWheelCanvas

            renderer.initWith(colorWheelRenderOption)
            renderer.draw()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.UNSPECIFIED -> widthMeasureSpec
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(widthMeasureSpec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(widthMeasureSpec)
            else -> widthMeasureSpec
        }
        val height = when (MeasureSpec.getMode(heightMeasureSpec)) {
            MeasureSpec.UNSPECIFIED -> heightMeasureSpec
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(heightMeasureSpec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(heightMeasureSpec)
            else -> heightMeasureSpec
        }
        val squareDimen: Int = if (height < width) {
            height
        } else {
            width
        }
        setMeasuredDimension(squareDimen, squareDimen)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                parent?.requestDisallowInterceptTouchEvent(true)
                onUpdateValueAfterEvent(event.x, event.y)
            }
            MotionEvent.ACTION_MOVE -> {
                onUpdateValueAfterEvent(event.x, event.y)
            }
            MotionEvent.ACTION_UP -> {
                hideInputKeyboard()
                this.requestFocus()

                currentColor = selectedColorInPicker
                setColorToSliders(currentColor)
                setColorToEditText(currentColor)
                onColorSelected()
                invalidate()
            }
        }
        return true
    }

    private fun onUpdateValueAfterEvent(eventX: Float, eventY: Float) {
        currentColorCircle = findNearestByPosition(eventX, eventY)
        currentColor = selectedColorInPicker
        setColorToSliders(currentColor)
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawColor(_backgroundColor)
        colorWheel?.run {
            canvas.drawBitmap(this, 0f, 0f, null)
        }
        currentColorCircle?.let { currColorCircle ->
            val maxRadius: Float = canvas.width * .5f - STROKE_RATIO *
                    (1f + ColorWheelRenderer.GAP_PERCENTAGE)
            val size: Float = maxRadius / density / 2
            colorWheelFill.color = Color.HSVToColor(
                currColorCircle.getHsvWithLightness(
                    lightness
                )
            )
            colorWheelFill.alpha = (_alpha * 0xff).toInt()
            canvas.drawCircle(
                currColorCircle.x,
                currColorCircle.y,
                size * STROKE_RATIO,
                selectorStroke1
            )
            canvas.drawCircle(
                currColorCircle.x,
                currColorCircle.y,
                size * (1 + (STROKE_RATIO - 1) / 2),
                selectorStroke2
            )
            canvas.drawCircle(
                currColorCircle.x,
                currColorCircle.y,
                size,
                alphaPatternPaint
            )
            canvas.drawCircle(
                currColorCircle.x,
                currColorCircle.y,
                size,
                colorWheelFill
            )
        }
    }

    private fun findNearestByPosition(
        x: Float,
        y: Float
    ): ColorCircle? {
        var near: ColorCircle? = null
        val circleList: List<ColorCircle> = renderer?.colorCircleList ?: return near

        var minDist: Double = Double.MAX_VALUE
        for (colorCircle in circleList) {
            val dist: Double = colorCircle.sqDist(x, y)
            if (minDist > dist) {
                minDist = dist
                near = colorCircle
            }
        }
        return near
    }

    private fun findNearestByColor(color: Int): ColorCircle? {
        var near: ColorCircle? = null
        val circleList: List<ColorCircle> = renderer?.colorCircleList ?: return near

        val hsv = FloatArray(3)
            .also { Color.colorToHSV(color, it) }
        var minDiff: Double = Double.MAX_VALUE
        val x: Double = hsv[1] * cos(hsv[0] * Math.PI / 180.0)
        val y: Double = hsv[1] * sin(hsv[0] * Math.PI / 180.0)
        var hsv1: FloatArray
        for (colorCircle in circleList) {
            hsv1 = colorCircle.hsv
            val x1: Double = hsv1[1] * cos(hsv1[0] * Math.PI / 180.0)
            val y1: Double = hsv1[1] * sin(hsv1[0] * Math.PI / 180.0)
            val dx: Double = x - x1
            val dy: Double = y - y1
            val dist: Double = dx * dx + dy * dy
            if (dist < minDiff) {
                minDiff = dist
                near = colorCircle
            }
        }
        return near
    }

    fun setLightness(lightness: Float) {
        this.lightness = lightness

        currentColor = selectedColorInPicker

        setColorToEditText(currentColor)
        alphaSlider?.setColor(currentColor)
        updateColorWheel()
    }

    fun setAlphaValue(alpha: Float) {
        _alpha = alpha
        currentColor = selectedColorInPicker

        setColorToEditText(currentColor)
        lightnessSlider?.setColor(currentColor)
        updateColorWheel()
    }

    @Throws(IllegalArgumentException::class)
    private fun setTextColor(colorText: String) {
        val color: Int = Color.parseColor(colorText)

        _alpha = color.alpha.toFloatColorComponent()
        lightness = FloatArray(3)
            .also { Color.colorToHSV(color, it) }
            .get(2)
        currentColorCircle = findNearestByColor(color)
        currentColor = color

        setColorToSliders(currentColor)
        setColorToEditText(currentColor)

        updateColorWheel()
    }

    fun setLightnessSlider(lightnessSlider: LightnessSlider?) {
        this.lightnessSlider = lightnessSlider
            ?.also {
                it.colorPicker = this
                it.setColor(currentColor)
            }
    }

    fun setAlphaSlider(alphaSlider: AlphaSlider?) {
        this.alphaSlider = alphaSlider
            ?.also {
                it.colorPicker = this
                it.setColor(currentColor)
            }
    }

    fun setColorEdit(colorEdit: EditText?) {
        this.colorEdit = colorEdit
            ?.apply {
                this.setSingleLine()
                this.isAllCaps = true
                this.filters = arrayOf(
                    AllCaps(),
                    LengthFilter(if (isAlphaEnabled) 9 else 7)
                )
                this.setHorizontallyScrolling(false)
                this.setOnEditorEnterListener(this@ColorPickerView)
                this.inputType = this.inputType or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            }
        setColorToEditText(currentColor)
    }

    fun setDensity(density: Int) {
        this.density = max(2, density)
        invalidate()
    }

    fun setRenderer(renderer: ColorWheelRenderer?) {
        this.renderer = renderer
        invalidate()
    }

    @SuppressLint("SetTextI18n")
    private fun setColorToEditText(argb: Int) {
        colorEdit?.setText(
            if (isAlphaEnabled) argb.toArgbString() else argb.toRgbString()
        )
    }

    private fun setColorToSliders(color: Int) {
        lightnessSlider?.setColor(color)
        alphaSlider?.setColor(color)
    }

    internal fun onColorSelected() {
        try {
            onColorChangedListener?.onColorChanged(currentColor)
        } catch (e: Exception) { // Squash individual listener exceptions
        }
    }

    internal fun hideInputKeyboard() {
        colorEdit?.let {
            it.clearFocus()
            context.hideKeyboard(it)
        }
    }

    private fun changeCurrentColorCircleIndex(indexShift: Int) {
        currentColorCircle = renderer?.colorCircleList?.let {
            val currentIndex = it.indexOf(currentColorCircle)
            it.getOrNull(currentIndex + indexShift)
                ?: if (currentIndex == 0) it.last() else it.first()
        }
        currentColor = selectedColorInPicker

        hideInputKeyboard()
        this.requestFocus()
        setColorToSliders(currentColor)
        setColorToEditText(currentColor)
        onColorSelected()
        invalidate()
    }

    override fun onEnter(view: TextView) {
        colorEdit?.let { edit ->
            hideInputKeyboard()
            lightnessSlider?.requestFocus()
            try {
                setTextColor(edit.text.toString())
                onColorSelected()
            } catch (e: Exception) {
                e.printStackTrace()
                setColorToEditText(currentColor)
            }
        }
    }

    /**
     * Important to intercept it here to avoid focus change
     */
    override fun dispatchKeyEvent(event: KeyEvent?): Boolean =
        when (event?.keyCode){
            KeyEvent.KEYCODE_DPAD_LEFT -> {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    changeCurrentColorCircleIndex(-1)
                }
                true
            }
            KeyEvent.KEYCODE_DPAD_RIGHT -> {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    changeCurrentColorCircleIndex(+1)
                }
                true
            }
            else -> super.dispatchKeyEvent(event)
        }

//--------------------------------------------------------------------------------------------------
//  Save/restore
//--------------------------------------------------------------------------------------------------

    override fun onSaveInstanceState(): Parcelable? {
        val superState: Parcelable = super.onSaveInstanceState() ?: return null

        val st = SavedState(superState)
        st.currentColor = this.currentColor

        return st
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val st: SavedState = state as? SavedState ?: return
        super.onRestoreInstanceState(st)

        this.currentColor = st.currentColor

        setColorToSliders(currentColor)
        setColorToEditText(currentColor)
        currentColorCircle = findNearestByColor(currentColor)
        invalidate()
    }

    class SavedState(superState: Parcelable) : BaseSavedState(superState) {
        internal var currentColor: Int = 0
    }

    @IntDef(
        WheelType.FLOWER,
        WheelType.CIRCLE
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class WheelType {
        companion object {
            const val FLOWER = 0
            const val CIRCLE = 1
        }
    }

    companion object {
        private const val STROKE_RATIO = 2f

        fun getRenderer(@WheelType wheelType: Int): ColorWheelRenderer =
            if (wheelType == WheelType.CIRCLE) {
                SimpleColorWheelRenderer()
            } else {
                FlowerColorWheelRenderer()
            }

        fun createAlphaPatternShader(size: Int): Shader = BitmapShader(
            createAlphaBackgroundPattern(
                max(8, size)
            ),
            Shader.TileMode.REPEAT,
            Shader.TileMode.REPEAT
        )

        private fun createAlphaBackgroundPattern(size: Int): Bitmap {
            val alphaPatternPaint = Paint()
            val bm: Bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
            val c = Canvas(bm)
            val s: Int = (size * .5f).roundToInt()
            for (i in 0..1) for (j in 0..1) {
                if ((i + j) % 2 == 0) {
                    alphaPatternPaint.color = Color.WHITE
                } else {
                    alphaPatternPaint.color = 0xffd0d0d0.toInt()
                }
                c.drawRect(
                    i * s.toFloat(),
                    j * s.toFloat(),
                    (i + 1) * s.toFloat(),
                    (j + 1) * s.toFloat(),
                    alphaPatternPaint
                )
            }
            return bm
        }
    }
}