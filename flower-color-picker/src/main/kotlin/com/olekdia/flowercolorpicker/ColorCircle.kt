package com.olekdia.flowercolorpicker

import android.graphics.Color

class ColorCircle(x: Float, y: Float, hsv: FloatArray) {

    var x = 0f
        private set
    var y = 0f
        private set
    var color = Color.TRANSPARENT
        private set
    val hsv = FloatArray(3)

    private var hsvClone: FloatArray? = null

    init {
        set(x, y, hsv)
    }

    fun sqDist(x: Float, y: Float): Double {
        val dx: Double = this.x - x.toDouble()
        val dy: Double = this.y - y.toDouble()
        return dx * dx + dy * dy
    }

    fun getHsvWithLightness(lightness: Float): FloatArray =
        (hsvClone ?: hsv.clone()).let { clone ->
            clone[0] = hsv[0]
            clone[1] = hsv[1]
            clone[2] = lightness
            hsvClone = clone

            clone
        }

    operator fun set(x: Float, y: Float, hsv: FloatArray) {
        this.x = x
        this.y = y
        this.hsv[0] = hsv[0]
        this.hsv[1] = hsv[1]
        this.hsv[2] = hsv[2]
        color = Color.HSVToColor(this.hsv)
    }
}