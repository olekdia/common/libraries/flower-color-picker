package com.olekdia.flowercolorpicker.renderer

import android.graphics.Canvas

class ColorWheelRenderOption {
    @JvmField
    var density = 0
    @JvmField
    var maxRadius = 0f
    @JvmField
    var cSize = 0f
    @JvmField
    var strokeWidth = 0f
    @JvmField
    var alpha = 0f
    @JvmField
    var lightness = 0f
    @JvmField
    var targetCanvas: Canvas? = null
}