package com.olekdia.flowercolorpicker.renderer

import com.olekdia.flowercolorpicker.ColorCircle

interface ColorWheelRenderer {
    val renderOption: ColorWheelRenderOption

    val colorCircleList: List<ColorCircle>

    fun initWith(colorWheelRenderOption: ColorWheelRenderOption)

    fun draw()

    companion object {
        const val GAP_PERCENTAGE = 0.025f
    }
}