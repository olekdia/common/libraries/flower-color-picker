package com.olekdia.flowercolorpicker.renderer

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.olekdia.flowercolorpicker.ColorCircle
import kotlin.math.cos
import kotlin.math.sin

class SimpleColorWheelRenderer : AbsColorWheelRenderer() {

    private val selectorFill = Paint()
    private val hsv = FloatArray(3)

    override fun draw() {
        val rOption: ColorWheelRenderOption = renderOption
        val targetCanvas: Canvas = rOption.targetCanvas ?: return

        val setSize: Int = colorCircleList.size
        var currentCount: Int = 0
        val half: Float = targetCanvas.width * .5f
        val density = rOption.density
        val maxRadius: Float = rOption.maxRadius

        for (i in 0 until density) {
            val p: Float = i.toFloat() / (density - 1) // 0~1
            val radius: Float = maxRadius * p
            val cSize: Float = rOption.cSize
            val total: Int = calcTotalCount(radius, cSize)

            for (j in 0 until total) {
                val angle: Double = Math.PI * 2 * j / total + Math.PI / total * ((i + 1) % 2)
                val x: Float = half + (radius * cos(angle)).toFloat()
                val y: Float = half + (radius * sin(angle)).toFloat()

                hsv[0] = (angle * 180f / Math.PI).toFloat()
                hsv[1] = radius / maxRadius
                hsv[2] = rOption.lightness
                selectorFill.color = Color.HSVToColor(hsv)
                selectorFill.alpha = alphaValueAsInt
                targetCanvas.drawCircle(
                    x,
                    y,
                    cSize - rOption.strokeWidth,
                    selectorFill
                )
                if (currentCount >= setSize) {
                    colorCircleList.add(ColorCircle(x, y, hsv))
                } else {
                    colorCircleList[currentCount][x, y] = hsv
                }
                currentCount++
            }
        }
    }
}