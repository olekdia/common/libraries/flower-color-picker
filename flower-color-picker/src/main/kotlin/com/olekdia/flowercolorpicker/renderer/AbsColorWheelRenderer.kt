package com.olekdia.flowercolorpicker.renderer

import com.olekdia.flowercolorpicker.ColorCircle
import com.olekdia.flowercolorpicker.renderer.ColorWheelRenderer.Companion.GAP_PERCENTAGE
import java.util.*
import kotlin.math.asin
import kotlin.math.max

abstract class AbsColorWheelRenderer : ColorWheelRenderer {

    @JvmField
    protected var colorWheelRenderOption: ColorWheelRenderOption? = null

    final override var colorCircleList: MutableList<ColorCircle> = ArrayList()
        private set

    override fun initWith(colorWheelRenderOption: ColorWheelRenderOption) {
        this.colorWheelRenderOption = colorWheelRenderOption
        this.colorCircleList.clear()
    }

    override val renderOption: ColorWheelRenderOption
        get() = colorWheelRenderOption
            ?: ColorWheelRenderOption()
                .also { colorWheelRenderOption = it }

    protected val alphaValueAsInt: Int
        get() = Math.round(renderOption.alpha * 255)

    protected fun calcTotalCount(radius: Float, size: Float): Int {
        return max(
            1,
            ((1f - GAP_PERCENTAGE) * Math.PI / asin(size / radius.toDouble()) + 0.5f).toInt()
        )
    }
}