package com.olekdia.sample;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.olekdia.flowercolorpicker.ColorPickerView;
import com.olekdia.flowercolorpicker.OnColorChangedListener;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class SampleActivity extends AppCompatActivity implements OnColorChangedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sample);
        final ColorPickerView picker = findViewById(R.id.fcp_color_picker);
        findViewById(R.id.fcp_alpha_slider).setVisibility(View.GONE);
        picker.setInitialColor(Color.BLUE);
        picker.setOnColorChangedListener(this);
    }

    @Override
    public void onColorChanged(int newColor) {
        ToastHelper.showToast(
            this,
            "Selected color: #" + Integer.toHexString(newColor).toUpperCase(Locale.US)
        );
    }
}