package com.olekdia.sample;

import android.content.Context;
import android.widget.Toast;

public class ToastHelper {

    private static Toast sToast;

    public static void showToast(final Toast toast) {
        cancelToast();
        sToast = toast;
        sToast.show();
    }

    public static void cancelToast() {
        if (sToast != null) {
            sToast.cancel();
        }
        sToast = null;
    }

    public static void showToast(final Context context, final CharSequence text) {
        cancelToast();
        sToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        sToast.show();
    }
}