rootProject.name = "flower-color-picker-android"

include(
    ":flower-color-picker",
    ":sample"
)